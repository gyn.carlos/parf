<?php

require_once __DIR__ . "/vendor/autoload.php";

use Phalcon\Di\FactoryDefault\Cli as CliDI;
use Phalcon\Cli\Console as ConsoleApp;
use Phalcon\Loader;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\Stream;

$di = new CliDI();

// config
$config = require_once __DIR__ . '/app.config.php';
$di->set('config', $config);

// config
$loader = (new \Phalcon\Loader())->registerDirs([
    'App\Controllers' => $config->application->controllersDir,
    'App\Models' => $config->application->modelsDir,
    'App\Util' => $config->application->utilDir,
    'App\Tasks' => $config->application->tasksDir,
])->register();

$di->get('dispatcher')->setDefaultNamespace('App\Tasks');

// logger
$di->set('logger', function () use ($config) {
    $adapter = new Stream($config->application->logsDir. date('Y-m-d') .".txt");
    return new Logger('messages',['main' => $adapter]);
});

// mysql
$di->setShared('mysql', function() use ($config) {
    return new \Phalcon\Db\Adapter\Pdo\Mysql([
        "host"     => $config->mysql->host,
        "username" => $config->mysql->username,
        "password" => $config->mysql->password,
        "dbname"   => $config->mysql->dbname,
        "port"     => $config->mysql->port
    ]);
});

// rabbitmq
$di->setShared('rabbit', function() use ($config) {
    $connection = new \PhpAmqpLib\Connection\AMQPConnection(
        $config->rabbit->host, 
        $config->rabbit->port, 
        $config->rabbit->username,
        $config->rabbit->password
    );
    return $connection;
});

// redis
$di->setShared('redis', function() use ($config) {
    $connection = new \Redis;
    $connection->connect($config->redis->host, $config->redis->port);
    $connection->auth($config->redis->password);
    return $connection;
});

// Create a console application
$console = new ConsoleApp();
$console->setDI($di);

//Process the console arguments
$arguments = [];
foreach ($argv as $k => $arg) {
    if ($k == 1) {
        $arguments["task"] = $arg;
    } elseif ($k == 2) {
        $arguments["action"] = $arg;
    } elseif ($k >= 3) {
        $arguments["params"][] = $arg;
    }
}

try {
    // Handle incoming arguments
    $console->handle($arguments);
} catch (\Phalcon\Exception $e) {
    echo $e->getMessage();
    exit(255);
}