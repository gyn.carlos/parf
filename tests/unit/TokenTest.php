<?php 

namespace App\Util;

class TokenTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testGenerateToken()
    {
        $tokenGenerate = (new Token())->generate();
        
        $this->assertJson($tokenGenerate,"Gera��o do token");

        $tokenFile = \codecept_data_dir() . "fixtures/jwt_token.json";

        \file_put_contents($tokenFile, $tokenGenerate);

        $this->assertFileExists($tokenFile, "Verifica se arquivo de token foi criado");
    }

    /**
     * @depends testGenerateToken
     */
    public function testValidateToken()
    {
        $tokenFile = \codecept_data_dir() . "fixtures/jwt_token.json";

        $this->assertFileExists($tokenFile, "Verifica se arquivo de token existe");

        $token = \json_decode(file_get_contents($tokenFile), true);

        $this->assertArrayHasKey("access_token",$token);

        $token = $token['access_token'];

        $validateToken = (new Token())->validate($token);
        
        $this->assertTrue($validateToken,true);
    }
}