<?php 

namespace App\Util;

class ExtractZipTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests

    public function testGetNameWithOutExtension()
    {
        $origin = codecept_data_dir("fixtures/PHP_CHALLENGE_CORRIGIDO.zip");

        $this->assertEquals((new FileExtract())->getNameWithOutExtension($origin),"PHP_CHALLENGE_CORRIGIDO");
    }
    
    public function testExtractFilesPeopleAndShipOrders()
    {
        $origin = codecept_data_dir("fixtures/PHP_CHALLENGE_CORRIGIDO.zip");
        $dest   = codecept_data_dir("fixtures");
        $files  = ["people.xml","shiporders.xml"];
        $expected = (new FileExtract)->extractTo($origin,$dest,$files);
        
        $dirExtration = $dest . "/PHP_CHALLENGE_CORRIGIDO";
        $this->assertDirectoryExists($dirExtration, "Verifica se os arquivos foram estraidos para o novo diretorio");
        $this->assertFileExists($dirExtration.'/'.$files[0], "Verifica se o people.xml existe no novo diretorio criado");
        $this->assertFileExists($dirExtration.'/'.$files[1], "Verifica se o shiporders.xml existe no novo diretorio criado");
    }
}