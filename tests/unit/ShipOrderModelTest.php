<?php 

namespace App\Models;

use App\Util\XMLParser;

use App\Models\ShipOrderModel;

class ShipOrderModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {

    }

    protected function _after()
    {

    }

    // tests
    public function testShipOrderBulking()
    {
        // Converte para Array
        $file = \codecept_data_dir() . "fixtures/PHP_CHALLENGE_CORRIGIDO/shiporders.xml";
        
        $shiporders = ((new XMLParser())->toArray($file));
        
        $this->assertArrayHasKey("shiporder",$shiporders, "Arquivo informado não corresponde ao XML de Shiporders");
        
        $fileNameUpload = 'UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw';

        $result = (new ShipOrderModel)->insert($shiporders, $fileNameUpload);

        $this->assertTrue($result, "Bulking import na tabela shiporders");
    }

    /**
     * @depends testShipOrderBulking
     */

    // tests
    public function testGetRowsImport()
    {
        $fileNameUpload = 'UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw';

        $expectRows = 4;

        $resultRows = sizeof((new ShipOrderModel)->getRows($fileNameUpload));

        $this->assertEquals($expectRows, $resultRows);
    }

}