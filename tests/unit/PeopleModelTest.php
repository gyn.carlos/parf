<?php 

namespace App\Models;

use App\Util\XMLParser;

use App\Models\PeopleModel;

class PeopleModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {

    }

    protected function _after()
    {

    }

    // tests
    public function testPeopleBulking()
    {
        // Converte para Array
        $file = \codecept_data_dir() . "fixtures/PHP_CHALLENGE_CORRIGIDO/people.xml";
        
        $persons = ((new XMLParser())->toArray($file));

        $this->assertArrayHasKey("person",$persons, "Arquivo informado não corresponde ao XML de People");
        
        $fileNameUpload = 'UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw';
        
        $result = (new PeopleModel)->insert($persons, $fileNameUpload);

        $this->assertTrue($result, "Bulking import na tabela people");
    }

    /**
     * @depends testPeopleBulking
     */

    // tests
    public function testGetRowsImport()
    {
        $fileNameUpload = 'UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw';

        $expectRows = 3;

        $resultRows = sizeof((new PeopleModel)->getRows($fileNameUpload));        

        $this->assertEquals($expectRows, $resultRows);
    }
}