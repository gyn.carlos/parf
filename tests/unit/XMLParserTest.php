<?php 

namespace App\Util;

class XMLParserTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testXMLPeople()
    {
        $obj = new XMLParser();

        $dest = codecept_data_dir("fixtures/PHP_CHALLENGE_CORRIGIDO");

        $peoples = $obj->toArray($dest . "/people.xml");

        $this->assertArrayHasKey("person",$peoples);
    }

    public function testXMLShipOrders()
    {
        $obj = new XMLParser();

        $dest = codecept_data_dir("fixtures/PHP_CHALLENGE_CORRIGIDO");

        $shiporders = $obj->toArray($dest . "/shiporders.xml");

        $this->assertArrayHasKey("shiporder",$shiporders);
    }
}