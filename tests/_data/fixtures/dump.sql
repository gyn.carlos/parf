CREATE TABLE IF NOT EXISTS `people` (
  `personid` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `personname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cell_phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name_upload` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`personid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `shiporders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `orderperson` int(11) NOT NULL,
  `shipto_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipto_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipto_city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipto_country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_note` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_quantity` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_price` decimal(10,2) DEFAULT NULL,
  `file_name_upload` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


TRUNCATE `people`;
TRUNCATE `shiporders`;


INSERT INTO bulk.people
(personid, personname, phone, cell_phone, file_name_upload)
VALUES(1, 'Name 1', '2345678', '1234567', 'UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw');
INSERT INTO bulk.people
(personid, personname, phone, cell_phone, file_name_upload)
VALUES(2, 'Name 2', '4444444', '', 'UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw');
INSERT INTO bulk.people
(personid, personname, phone, cell_phone, file_name_upload)
VALUES(3, 'Name 3', '7777777', '8888888', 'UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw');


INSERT INTO bulk.shiporders
(id, orderid, orderperson, shipto_name, shipto_address, shipto_city, shipto_country, item_title, item_note, item_quantity, item_price, file_name_upload)
VALUES(1, 1, 1, 'Name 1', 'Address 1', 'City 1', 'Country 1', 'Title 1', 'Note 1', '745', 123.45, 'UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw');
INSERT INTO bulk.shiporders
(id, orderid, orderperson, shipto_name, shipto_address, shipto_city, shipto_country, item_title, item_note, item_quantity, item_price, file_name_upload)
VALUES(2, 2, 2, 'Name 2', 'Address 2', 'City 2', 'Country 2', 'Title 2', 'Note 2', '45', 13.45, 'UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw');
INSERT INTO bulk.shiporders
(id, orderid, orderperson, shipto_name, shipto_address, shipto_city, shipto_country, item_title, item_note, item_quantity, item_price, file_name_upload)
VALUES(3, 3, 3, 'Name 3', 'Address 3', 'City 3', 'Country 3', 'Title 3', 'Note 3', '5', 1.12, 'UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw');
INSERT INTO bulk.shiporders
(id, orderid, orderperson, shipto_name, shipto_address, shipto_city, shipto_country, item_title, item_note, item_quantity, item_price, file_name_upload)
VALUES(4, 3, 3, 'Name 3', 'Address 3', 'City 3', 'Country 3', 'Title 4', 'Note 4', '2', 77.12, 'UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw');
