<?php

use Codeception\Util\HttpCode as responseValidate;

use \PhpAmqpLib\Message\AMQPMessage;

use App\Tasks\ConsumeFileTask;

use Phalcon\Cli\Task;

use Phalcon\Di;

class ProcessFileCest
{
    private function encodeName($baseName)
    {
        return base64_encode($baseName);
    }

    public function testCleanDbRedis($I)
    {
        $I->wantTo('Clearing redis data');
        $I->sendCommandToRedis('flushall');
    }

    /**
     * @depends testCleanDbRedis
     */

    public function testUploadFile($I)
    {        
        $name = 'PHP_CHALLENGE_CORRIGIDO.zip';
        $encodeName = $this->encodeName($name);

        $fileDir = codecept_data_dir("fixtures/".$name);

        $file = [
            'file_upload'=> [
                'name'=>$name,
                'type'=>'image/png',
                'size'=>filesize($fileDir),
                'tmp_name'=>$fileDir,
                'error'=> UPLOAD_ERR_OK
            ]
        ];

        $accessToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7Im5hbWUiOiJDYXJsb3MgQXVndXN0byIsImFkbWluIjp0cnVlfSwiaXNzIjoiaHR0cDpcL1wvbG9jYWxob3N0OjgwMjAiLCJzdWIiOiIxNDI1MzYifQ.XJRbGBUqVbOhv9X_aNgUaN-g9hJXlmqmWYD3GwNTdjY';

        $I->wantTo('Sending the file via POST');
        $I->haveHttpHeader(
            'access-token',$accessToken
        );
        $I->sendPOST('/upload', [], $file);
        $I->seeResponseCodeIs(responseValidate::ACCEPTED);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"filename":"'.$encodeName.'"}');
    }

    /**
     * @depends testUploadFile
     */

    public function testCheckStatusUpload($I)
    {
        $fileNameUpload = "UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw";

        $accessToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7Im5hbWUiOiJDYXJsb3MgQXVndXN0byIsImFkbWluIjp0cnVlfSwiaXNzIjoiaHR0cDpcL1wvbG9jYWxob3N0OjgwMjAiLCJzdWIiOiIxNDI1MzYifQ.XJRbGBUqVbOhv9X_aNgUaN-g9hJXlmqmWYD3GwNTdjY';

        $I->wantTo('Checking the status of the uploaded file');
        $I->haveHttpHeader(
            'access-token',$accessToken
        );
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendGET("/upload/{$fileNameUpload}", []);
        $I->seeResponseCodeIs(responseValidate::ACCEPTED);
        $I->seeResponseIsJson();
        $I->seeResponseIsJson('{"status":"AGUARDANDO_PROCESSAMENTO"}');
    }

    /**
     * @depends testCheckStatusUpload
     */

    public function testShowRowsPeople($I)
    {
        $fileNameUpload = "UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw";

        $accessToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7Im5hbWUiOiJDYXJsb3MgQXVndXN0byIsImFkbWluIjp0cnVlfSwiaXNzIjoiaHR0cDpcL1wvbG9jYWxob3N0OjgwMjAiLCJzdWIiOiIxNDI1MzYifQ.XJRbGBUqVbOhv9X_aNgUaN-g9hJXlmqmWYD3GwNTdjY';

        $I->wantTo('Lists records that have been imported for people');
        $I->haveHttpHeader(
            'access-token',$accessToken
        );
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendGET("/people/{$fileNameUpload}", []);
        $I->seeResponseCodeIs(responseValidate::OK);
        $I->seeResponseIsJson();
    }

    /**
     * @depends testShowRowsPeople
     */

    public function testShowRowsShipOrder($I)
    {
        $fileNameUpload = "UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw";

        $accessToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7Im5hbWUiOiJDYXJsb3MgQXVndXN0byIsImFkbWluIjp0cnVlfSwiaXNzIjoiaHR0cDpcL1wvbG9jYWxob3N0OjgwMjAiLCJzdWIiOiIxNDI1MzYifQ.XJRbGBUqVbOhv9X_aNgUaN-g9hJXlmqmWYD3GwNTdjY';

        $I->wantTo('Lists records that have been imported for shiporder');
        $I->haveHttpHeader(
            'access-token',$accessToken
        );
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendGET("/shiporder/{$fileNameUpload}", []);
        $I->seeResponseCodeIs(responseValidate::OK);
        $I->seeResponseIsJson();
    }
}