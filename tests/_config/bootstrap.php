<?php

use Codeception\Lib\Connector\Phalcon\MemorySession as CodeceptionMemorySession;
use Phalcon\Config;
use Phalcon\Di\FactoryDefault;
use Phalcon\Loader;
use Phalcon\Mvc\Application;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\View;
use Phalcon\Test\Fixtures\MemorySession as PhalconMemorySession;
use Phalcon\Url;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\Stream;


$container = new FactoryDefault();

/**
 * Load environment
 */
$root = dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR;

/**
 * Config
 */

$config = [
    'application' => [
        'appDir'         => $root . '/',
        'controllersDir' => $root . '/Controllers/',
        'tasksDir'       => $root . '/Tasks/',
        'modelsDir'      => $root . '/Models/',
        'utilDir'        => $root . '/Util/',
        'middlewaresDir' => $root . '/Middlewares/',        
        'migrationsDir'  => $root . '/Migrations/',
        'libraryDir'     => $root . '/Library/',
        'uploadDir'      => $root . '/uploads/',
        'logsDir'        => $root . '/logs/',
        'baseUri'        => '/src/',
    ],
    'mysql' => [
        'adapter'     => 'Mysql',
        'host'        => '172.30.0.2',
        'username'    => 'root',
        'password'    => 'mysql3241',
        'dbname'      => 'bulk',
        'charset'     => 'utf8mb4',
    ],
    'rabbit' => [
        'host' => '172.30.0.3',
        'port' => '5672',
        'username' => 'rabbit',
        'password' => 'rabbit3241',
        'queueName' => 'UploadFile',
        'exchange'  => 'SendFileApi'
    ],
    'redis' => [
        'host' => '172.30.0.4',
        'port' => '6379',
        'password' => 'redis3241'
    ]
];

$config = new \Phalcon\Config($config);

$container->setShared('config', $config);

// logger
$container->set('logger', function () use ($config) {
    $adapter = new Stream($config->application->logsDir. date('Y-m-d') .".txt");
    return new Logger('messages',['main' => $adapter]);
});

// mysql
$container->setShared('mysql', function() use ($config) {
    return new \Phalcon\Db\Adapter\Pdo\Mysql([
        "host"     => $config->mysql->host,
        "username" => $config->mysql->username,
        "password" => $config->mysql->password,
        "dbname"   => $config->mysql->dbname,
        "port"     => $config->mysql->port
    ]);
});

// rabbitmq
$container->setShared('rabbit', function() use ($config) {
    $connection = new \PhpAmqpLib\Connection\AMQPConnection(
        $config->rabbit->host, 
        $config->rabbit->port, 
        $config->rabbit->username,
        $config->rabbit->password
    );
    return $connection;
});

// redis
$container->setShared('redis', function() use ($config) {
    $connection = new \Redis;
    $connection->connect($config->redis->host, $config->redis->port);
    $connection->auth($config->redis->password);
    return $connection;
});

/**
 * Dispatcher
 */

$application = new Application();

$application->setDI($container);

FactoryDefault::setDefault($container);

return $application;