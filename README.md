# PFAR - Process File Async and Rest

![](./screenshots/Main.gif)

## Conteúdo

- [About](#about)
- [O que foi pedido?](#O-que-foi-pedido)
- [O que foi entregue?](#O-que-foi-entregue)
- [Requisitos](#requisitos)
- [Desenvolvimento](#desenvolvimento)
    - [Frontend](#fronted)
    - [Backend](#backend)
    - [Arquitetura](#arquitetura)
    - [Boas práticas](#boas-práticas)
    - [Screenshots](#screenshots)
    - [Performance](#performance)
- [Instalação/Configuração](#instalação)
    - [Configurando docker para rodar sem sudo](#configurando-docker)
    - [Instalando e configurando a aplicação](#instalando-e-configurando-a-aplicação)
- [Operação](#operação)
    - [Formulário de envio](#formulário-de-envio)
    - [REST Endpoints](#rest-endpoints)

## About

O objetivo do projeto é permitir a importação maciça de dados de maneira não bloqueante (async) e disponibilizar uma API Rest para consulta posterior dos itens que foram processados e inseridos no banco de dados. O projeto foi feito para [**Invillia**](https://invillia.com) . 

## O que foi pedido?

- Interface para upload de arquivos (obrigatório);
- Importação maciça de dados de maneira não bloqueante (async) (obrigatório);
- Validação de inputs (obrigatório);
- Testes em qualquer nível (obrigatório);
- Definição arquitetural (obrigatório);
- README.md do projeto (obrigatório);
- Autorização (bônus);
- Documentação (bônus);

## O que foi entregue?

- Interface para upload de arquivos;
- Importação maciça de dados de maneira não bloqueante (async);
- Validação de inputs;
- Testes em qualquer nível (unitário e funcional);
- Definição arquitetural (MVCS);
- README.md do projeto;
- Autorização (JWT);
- Documentação (Swagger);
- Log application;

## Requisitos

- curl >= 7.64.0
- jq >= 1.5-1
- Docker >= 19
- Docker-compose >= 1.25.4
- php 7
- composer >= 1.8.4

## Desenvolvimento

### Frontend

- Javascript/jQuery
- Bootstrap
- CSS

### Backend

- Linux
- Docker (image [**gyncarlos/web-server**](https://hub.docker.com/r/gyncarlos/web-server))
- Apache 2.4
- PHP 7.4 (mod apache e cli)
- Phalcon 4
- MYSQL
- RabbitMQ (para filas de processamento)
- Redis (cache dos envios)
- CodeException (TDD)

### Arquitetura

- MVCS (Model-View-Controller-Service)

### Boas práticas

- TDD
- DDD
- Clean Code
- Log application
- Autorização
- Documentação

### Screenshots

![](./screenshots/Tests.png)

![](./screenshots/QueueRabbit.png)

![](./screenshots/ReadOnMinuteRabbit.png)

![](./screenshots/People.png)

![](./screenshots/Shiporder.png)

### Performance

- Uso da extensão compilada https://github.com/cdoco/php-jwt para geração e validação dos tokens (JWT);
- OPCache ativado;
- Inserting multiple rows;
> **ATENÇÃO**: O Layout dos arquivos e a modelagem do banco não possibilitaram o uso do [*LOAD DATA INFILE*](https://dev.mysql.com/doc/refman/8.0/en/load-data.html) que é o mais indicado para a realização de um bulk import


## Instalação

### Configurando docker

```bash
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
# executou sem pedir senha?
docker ps
```
> O populator do unit.suite.yml funciona melhor sem sudo (caso contrário ele solicitará senha)

### Instalando e configurando a aplicação

```bash
#baixar o repositorio
git clone https://gitlab.com/gyn.carlos/parf.git
#entrar no diretorio
cd parf
#aplicar as permissoes
sudo ./init.sh
#instalar as dependencias
composer update
#subindo os serviços
sudo docker-compose up -d
```

![](./screenshots/ContainersOnline.png)

## Operação

### Formulário de envio

- http://localhost:8020/web/

### REST Endpoints

> Acessível também com Insomnia/Postman

#### File upload

```bash
curl -X POST http://localhost:8020/upload \
-H 'access-token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7Im5hbWUiOiJDYXJsb3MgQXVndXN0byIsImFkbWluIjp0cnVlfSwiaXNzIjoiaHR0cDpcL1wvbG9jYWxob3N0OjgwMjAiLCJzdWIiOiIxNDI1MzYifQ.XJRbGBUqVbOhv9X_aNgUaN-g9hJXlmqmWYD3GwNTdjY' \
-F 'image=@./tests/_data/fixtures/PHP_CHALLENGE_CORRIGIDO.zip' | jq
```  

![](./screenshots/CurlUpload.png)

#### Consulta status do processamento

```bash
curl http://localhost:8020/upload/UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw \
-H 'access-token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7Im5hbWUiOiJDYXJsb3MgQXVndXN0byIsImFkbWluIjp0cnVlfSwiaXNzIjoiaHR0cDpcL1wvbG9jYWxob3N0OjgwMjAiLCJzdWIiOiIxNDI1MzYifQ.XJRbGBUqVbOhv9X_aNgUaN-g9hJXlmqmWYD3GwNTdjY' | jq
```

![](./screenshots/CurlStatusProcessamento.png)


#### Consulta os dados processados de people.xml

```bash
curl http://localhost:8020/people/UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw \
-H 'access-token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7Im5hbWUiOiJDYXJsb3MgQXVndXN0byIsImFkbWluIjp0cnVlfSwiaXNzIjoiaHR0cDpcL1wvbG9jYWxob3N0OjgwMjAiLCJzdWIiOiIxNDI1MzYifQ.XJRbGBUqVbOhv9X_aNgUaN-g9hJXlmqmWYD3GwNTdjY' | jq
```

![](./screenshots/CurlPeople.png)

#### Consulta os dados processados de shiporder.xml

```bash
curl http://localhost:8020/shiporder/UEhQX0NIQUxMRU5HRV9DT1JSSUdJRE8uemlw \
-H 'access-token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7Im5hbWUiOiJDYXJsb3MgQXVndXN0byIsImFkbWluIjp0cnVlfSwiaXNzIjoiaHR0cDpcL1wvbG9jYWxob3N0OjgwMjAiLCJzdWIiOiIxNDI1MzYifQ.XJRbGBUqVbOhv9X_aNgUaN-g9hJXlmqmWYD3GwNTdjY' | jq
```  

![](./screenshots/CurlShiporder.png)
