<?php

namespace App\Models;

use App\Services\PeopleService;

class PeopleModel extends BaseModel
{
    public function insert(Array $persons, $fileNameUpload) : bool
    {
        $service = new PeopleService;

        $personsNormalized = $service->normalizeValues($persons, $fileNameUpload);

        $sql = $service->createInsertMultipleRows($personsNormalized);

        return $this->bulk($sql);
    }

    public function getRows($fileNameUpload) : Array
    {
        return $this->getRowsByFileName("people",$fileNameUpload);
    }
}