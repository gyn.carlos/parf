<?php

namespace App\Models;

use App\Services\ShipOrderService;

class ShipOrderModel extends BaseModel
{
    public function insert(Array $shipOrders, string $fileNameUpload) : bool
    {
        $service = new ShipOrderService;

        $shipOrdersNormalized = $service->normalizeValues($shipOrders,$fileNameUpload);

        $sql = $service->createInsertMultipleRows($shipOrdersNormalized);

        return $this->bulk($sql);
    }

    public function getRows(string $fileNameUpload) : Array
    {
        return $this->getRowsByFileName("shiporders",$fileNameUpload);
    }
}