<?php

namespace App\Models;

use Phalcon\Mvc\Model;

class BaseModel extends Model
{
    public function bulk($sql) : bool
    {
        try {
            $logger = $this->getDi()->getShared('logger');
            $connection = $this->getDi()->getShared('mysql');
            $connection->begin();
            $statment = $connection->prepare($sql);
            $result = $statment->execute();
            $connection->commit();
            return $result;
        } catch(\PDOException $e) {
            $logger->error($e->getMessage() . "\n" . $sql);
            $connection->rollback();
            return false;
        }    
    }

    public function getRowsByFileName(string $tableName,string $fileNameUpload) : Array
    {
        $this->db = $this->getDi()->getShared('mysql');
        $stmt = $this->db->prepare("SELECT * FROM ".$tableName." WHERE file_name_upload = :file_name_upload");
        $stmt->execute([':file_name_upload'=>$fileNameUpload]);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}