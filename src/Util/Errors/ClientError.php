<?php declare (strict_types=1);

namespace App\Util\Errors;

class ClientError extends \Exception 
{
    private $data = [];
    
    public function __construct($message, $code = 400, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function getData()
    {
        return $this->data;
    }

    public function getOutputMessage()
    {
        return $this->getMessage();
    }
}