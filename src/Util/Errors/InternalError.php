<?php declare (strict_types=1);

namespace App\Util\Errors;

class InternalError extends \Exception 
{
    private $data = [];

    private $messageDefault = "Algo deu errado";

    public function __construct($message, $code = 500, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function getData()
    {
        return $this->data;
    }

    public function getOutputMessage()
    {
        return $this->messageDefault;
    }
}