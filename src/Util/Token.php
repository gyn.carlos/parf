<?php

namespace App\Util;

class Token
{
    public static function generate()
    {
        try {
            $key = "microapi";
            $payload = [
                "data" => [
                    "name" => "Carlos Augusto",
                    "admin" => true
                ],
                "iss" => "http://localhost:8020",
                "sub" => "142536",
            ];
            
            $token = \Cdoco\JWT::encode($payload, $key);

            return \json_encode([
                "access_token" => $token,
                "token_type" => "Basic"
            ]);
        } catch(\Exception $e) {
            return false;
        }
    }

    public static function validate($token)
    {      
        try {
            $key = "microapi";
            $decoded_token = \Cdoco\JWT::decode($token, $key);
            if (empty($decoded_token)) {
                return false;
            }
            return true;
        } catch(\Exception $e) {
            return false;
        }
    }
}
