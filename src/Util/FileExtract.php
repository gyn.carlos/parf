<?php

namespace App\Util;

use App\Util\Errors\InternalError;

class FileExtract
{

    public function getNameWithOutExtension(string $fullName) : string
    {
        return substr($fullName,\strrpos($fullName,'/') + 1, -4);
    }
    
    public function extractTo($origin,$dest,Array $files = []) : string
    {
        $zip  = new \ZipArchive;

        if (!class_exists('ZipArchive')) {
            throw new InternalError ('Extension ZipArchive not installed');
        }

        if (\file_exists($origin) === false) {
            throw new InternalError ("O arquivo enviado {$origin} n�o existe");
        }

        if (\is_writable($dest) === false) {
            throw new InternalError ("O diret�rio {$dest} sem permiss�o");
        }

        if ($zip->open($origin) === false) {
            throw new InternalError ('N�o foi poss�vel extrair o arquivo');
        }

        $destination = $dest . '/' . $this->getNameWithOutExtension($origin);

        $zip->extractTo($destination,$files);

        $zip->close();

        return $destination;
    }
}