<?php

namespace App\Util;

use App\Util\Errors\InternalError;

class XMLParser
{
    public function toArray($fileName)
    {

        if (!class_exists('SimpleXMLElement')) {
            throw new InternalError("Extension XML not installed");
        }

        $file = realpath($fileName);

        if (!\is_readable($file)) {
            throw new InternalError("O arquivo $fileName n�o esta acess�vel");
        }

        libxml_use_internal_errors(true);

        $objXml = simplexml_load_file($file);
    
        if ($objXml === false) {

            throw new InternalError("O arquivo XML $fileName � inv�lido");
            
            foreach (libxml_get_errors() as $error) {
                throw new InternalError($error->message);
            }
        }
        
        $objJson  = json_encode($objXml);

        return json_decode($objJson, true);
    }
}