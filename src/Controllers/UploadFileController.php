<?php

namespace App\Controllers;

use Phalcon\Http\Response;
use Phalcon\Http\Request;

use App\Util\Errors\InternalError;
use App\Util\Errors\ClientError;

class UploadFileController extends BaseController
{
    const STATUS_CODE_FILEUPLOAD = [
        "AGUARDANDO_PROCESSAMENTO" => 202,
        "PROCESSAMENTO_CONCLUIDO"  => 201
    ];
    
    private function validateFile()
    {
        // Fez um request sem nada
        $this->files = $this->request->getUploadedFiles();
        if (empty($this->files)) {
            throw new ClientError ("Nenhum arquivo foi enviado");
        }

        if (sizeof($this->files) > 1) {
            throw new ClientError ('É permitido enviar apenas um arquivo por vez');
        }

        return $this;
    }

    private function encodeName($baseName)
    {
        return base64_encode($baseName);
    }

    public function upload()
    {
        try {

            $this->validateToken()->validateFile();
    
            // Move o arquivo
            $dirUpload = $this->config->application->uploadDir;
            foreach ($this->files as $file) {
                $extension = $file->getExtension();
                $baseName  = $file->getName();
                $file->moveTo($dirUpload . $this->encodeName($baseName));
            }

            // Envia o arquivo para fila de processamento
            if ((new PublisherController)->pushMessage($this->encodeName($baseName)) == false) {
                throw new InternalError ('Falha ao enviar o arquivo para a fila de processamento');
            }

            return $this->sendResponse(["filename"=>$this->encodeName($baseName)],202);

        } catch(\Exception $e) {
            $this->logger->error($e->getMessage(). "\nStackTrace: ". print_r($e->getTrace(), true));
            return $this->sendErrorResponse($e);
        }
    }

    public function status(string $fileNameUpload)
    {
        try {

            $this->validateToken();

            if (empty($fileNameUpload)) {
                throw new ClientError ("É obrigatório informar o campo fileNameUpload"); 
            }

            $redis = $this->getDi()->getShared('redis');
    
            if (false === $statusFileUpload = $redis->get($fileNameUpload)) {
                throw new ClientError ("Arquivo inválido ou inexistente");
            }
    
            return $this->sendResponse(["status" => $statusFileUpload], self::STATUS_CODE_FILEUPLOAD[$statusFileUpload]);

        } catch(\Exception $e) {
            $this->logger->error($e->getMessage(). "\nStackTrace: ". print_r($e->getTrace(), true));
            return $this->sendErrorResponse($e);
        }
    }
}