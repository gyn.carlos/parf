<?php

namespace App\Controllers;

use Phalcon\Http\Response;
use Phalcon\Http\Request;

use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Message\AMQPMessage;

use App\Util\Errors\InternalError;

use Phalcon\Mvc\Controller;
use App\Models\ShipOrderModel;

class ShipOrderController extends BaseController
{
    public function showRows($fileNameUpload)
    {
        try {

            $this->validateToken();

            if (empty($fileNameUpload)) {
                throw new ClientError ("� obrigat�rio informar o campo fileNameUpload"); 
            }
        
            $rows = (new ShipOrderModel)->getRows($fileNameUpload);
    
            return $this->sendResponse($rows);

        } catch(\Exception $e) {
            $this->logger->error($e->getMessage(). "\nStackTrace: ". print_r($e->getTrace(), true));
            return $this->sendErrorResponse($e);
        }
    }
}