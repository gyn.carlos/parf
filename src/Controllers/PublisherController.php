<?php

namespace App\Controllers;

use Phalcon\Http\Response;
use Phalcon\Http\Request;

use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Message\AMQPMessage;

use App\Util\Errors\InternalError;

use Phalcon\Mvc\Controller;

class PublisherController extends BaseController
{
    const STATUS_AGUARDANDO_PROCESSAMENTO = "AGUARDANDO_PROCESSAMENTO";
    
    public function setQueue()
    {
        // get channel
        $this->channel  = $this->getDi()->getShared('rabbit')->channel();

        // definindo fila padrao
        $exchange  = $this->config->rabbit->exchange;
        $queue     = $this->config->rabbit->queue;

        $this->exchange = $exchange;
        $this->queue    = $queue;

        // definindo fila reprocessamento
        $exchangeRetry = $this->config->rabbit->exchange_retry;
        $retryQueue    = $this->config->rabbit->queue_retry;

        $this->channel->exchange_declare($exchange, 'direct', false, true);
        $this->channel->exchange_declare($exchangeRetry, 'direct', false, true);

        // Fila normal e a da reenvio

        $this->channel->queue_declare($queue, false, true, false, false, false, new \PhpAmqpLib\Wire\AMQPTable([
            'x-dead-letter-exchange' => '',
            'x-dead-letter-routing-key' => $retryQueue
        ]));

        $this->channel->queue_bind($queue, $exchange);

        // A cada 120 segundos as mensagens da fila de reprocessamento ser�o lidas
        $ttlMessage = $this->config->rabbit->ttl_message;
        $this->channel->queue_declare($retryQueue, false, true, false, false, false, new \PhpAmqpLib\Wire\AMQPTable([
            'x-dead-letter-exchange' => '',
            'x-dead-letter-routing-key' => $queue,
            'x-message-ttl' => $ttlMessage
        ]));
        $this->channel->queue_bind($retryQueue, $exchangeRetry);

        return $this;
    }

    public function pushMessage(string $messageBody)
    {
        try {
            $this->pushRabbit($messageBody);
            $this->pushRedis($messageBody);
            return true;
        } catch(InternalError $e) {
            return false;
        }
    }

    private function pushRabbit($messageBody)
    {
        $this->setQueue();
        $message = new AMQPMessage($messageBody, [
            'content_type' => 'text/plain', 
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]
        );
        $this->channel->basic_publish($message, $this->exchange);
        $this->channel->close();
    }

    private function pushRedis($messageBody)
    {
        $redis = $this->getDi()->getShared('redis');
        $redis->set($messageBody,self::STATUS_AGUARDANDO_PROCESSAMENTO);
    }
}