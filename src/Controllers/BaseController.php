<?php

namespace App\Controllers;

use Phalcon\Mvc\Controller;
use Lukasoppermann\Httpstatus\Httpstatus as httpStatusCode;
use App\Util\Errors\ClientError;
use App\Util\Token;

class BaseController extends Controller
{
    public function validateToken()
    {
        if (empty($this->request->getHeaders()["Access-Token"])) {
            throw new ClientError ("Access token ausente");
        }

        $token = $this->request->getHeaders()["Access-Token"];

        if (Token::validate($token) === false) {
            throw new ClientError ("Access token � inv�lido",401);
        }

        return $this;
    }

    public function sendErrorResponse($e)
    {
        $response = [
            "statusCode" => $e->getCode(),
            "codeAsString" => (new HttpStatusCode)->getReasonPhrase($e->getCode()),
            "message" => $e->getOutputMessage(),
            "data" => $e->getData()
        ];

        $this->response->setStatusCode($e->getCode());
        $this->response->setContent(json_encode($response));
        $this->response->send();
    }

    public function sendResponse($data, $code = 200)
    {
        $response = [
            "statusCode" => $code,
            "codeAsString" => (new HttpStatusCode)->getReasonPhrase($code),
            "data" => $data
        ];
        
        $this->response->setStatusCode($code);
        $this->response->setContent(json_encode($response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        $this->response->send();
    }
}