<?php

namespace App\Services;

class PeopleService 
{
    private $table   = "people";
    private $columns = [
        "personid", "personname",
        "phone"   , "cell_phone"
    ];

    public function normalizeValues(Array $persons, string $fileNameUpload) : Array
    {
        if (empty($persons['person'])) {
            throw new InternalError ("O arquivo enviado n�o pertence a people");
        }

        if (empty($fileNameUpload)) {
            throw new InternalError ("Id do arquivo n�o foi informado");
        }

        $persons = $persons['person'];
        $normalized = [];
    
        foreach ($persons as $people) {
    
            $personid = $people['personid'];
            $personname = $people['personname'];
            // Por default s� tem um telefone
            $phone = $people['phones']['phone'];
            $cellPhone = '';

            // Quando tiver dois
            if (is_array($people['phones']['phone'])) {
                list($phone, $cellPhone) = $people['phones']['phone'];
            }

            // agrupa por id de pessoa
            if (!isset($normalized[$personid])) {
                $normalized[$personid] = new \stdClass;
            }

            $peopleLine = new \stdClass;
            $peopleLine->personid = $personid;
            $peopleLine->personname = $personname;
            $peopleLine->phone = $phone;
            $peopleLine->cell_phone = $cellPhone;
            $peopleLine->file_name_upload = $fileNameUpload;

            $normalized[$personid] = $peopleLine;
        }
        
        return $normalized;
    }

    public function createInsertMultipleRows(Array $personsNormalized) : string
    {
        // Montando os valores
        $values = '';
        foreach ($personsNormalized as $people) {

            $personid   = $people->personid;
            $personname = $people->personname;
            $phone      = $people->phone;
            $cellPhone  = $people->cell_phone;
            $fileNameUpload = $people->file_name_upload;
            $values .= ",($personid,'{$personname}','{$phone}','{$cellPhone}','{$fileNameUpload}')";
        }
        
        // Inserindo os dados
        $values  =  substr($values,1);
        $columns =  implode(",",$this->columns);
        $sql = "INSERT INTO " . $this->table. " "
             . "VALUES {$values} "
             . "ON DUPLICATE KEY UPDATE personid = VALUES(personid); ";
 
        return $sql;
    }
}