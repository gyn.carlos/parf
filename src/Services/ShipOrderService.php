<?php

namespace App\Services;

use App\Util\Errors\InternalError;

class ShipOrderService 
{
    private $table   = "shiporders";
    private $columns = [
        "orderid"       , "orderperson"    , 
        "shipto_name"   , "shipto_address" , "shipto_city"   , "shipto_country", 
        "item_title"    , "item_note"      , "item_quantity" , "item_price",
        "file_name_upload"
    ];

    public function normalizeValues(Array $shipOrders, $fileNameUpload) : Array
    {
        if (empty($shipOrders['shiporder'])) {
            throw new InternalError ("O arquivo enviado não pertence a shiporders");
        }

        if (empty($fileNameUpload)) {
            throw new InternalError ("Id do arquivo não foi informado");
        }

        $shiporders = $shipOrders['shiporder'];
        $normalized = [];

        foreach($shiporders as $key => $shiporder) {

            $orderid     = $shiporder['orderid'];
            $orderperson = $shiporder['orderperson'];
            $items       = $shiporder['items'];
            $shipto      = $shiporder['shipto'];

            // substituindo a posicao zero do array de item
            if (isset($items['item']['title'])) {
                $values = $items['item'];
                unset($shiporders[$key]['items']['item']);
                $shiporders[$key]['items']['item'][] = $values;
            }

            // colocando todos itens em uma linha só
            $itens = $shiporders[$key]['items']['item'];
            foreach($itens as $key => $item) {
                $shipOrder = new \stdClass;
                $shipOrder->orderid = $orderid;
                $shipOrder->orderperson = $orderperson;
                $shipOrder->shipto_name = $shipto['name'];
                $shipOrder->shipto_address = $shipto['address'];
                $shipOrder->shipto_city    = $shipto['city'];
                $shipOrder->shipto_country = $shipto['country'];
                $shipOrder->item_title     = $item['title'];
                $shipOrder->item_note      = $item['note'];
                $shipOrder->item_quantity  = $item['quantity'];
                $shipOrder->item_price     = $item['price'];
                $shipOrder->file_name_upload = $fileNameUpload;
                $normalized[] = $shipOrder;
            }
        }

        return $normalized;
    }

    public function createInsertMultipleRows(Array $shipOrdersNormalized) : string
    {
        // Montando os valores
        $values = '';
        foreach ($shipOrdersNormalized as $shipOrder) {
            $orderid        = $shipOrder->orderid;
            $orderperson    = $shipOrder->orderperson;
            $shipto_name    = $shipOrder->shipto_name;
            $shipto_address = $shipOrder->shipto_address;
            $shipto_city    = $shipOrder->shipto_city;
            $shipto_country = $shipOrder->shipto_country;
            $item_title     = $shipOrder->item_title;
            $item_note      = $shipOrder->item_note;
            $item_quantity  = $shipOrder->item_quantity;
            $item_price     = $shipOrder->item_price;
            $fileNameUpload = $shipOrder->file_name_upload;

            $values .= ",($orderid, $orderperson, '$shipto_name', '$shipto_address', '$shipto_city', '$shipto_country', '$item_title', '$item_note', $item_quantity, $item_price, '$fileNameUpload')";
        }
        
        // Inserindo os dados
        $values  =  substr($values,1);
        $columns =  implode(",",$this->columns);
        $sql     = "INSERT INTO {$this->table}({$columns}) VALUES {$values}";

        return $sql;
    }
}