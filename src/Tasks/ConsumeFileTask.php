<?php

namespace App\Tasks;

use Phalcon\Cli\Task;

use PhpAmqpLib\Exception\AMQPBasicCancelException;
use PhpAmqpLib\Exception\AMQPIOException;

use App\Util\FileExtract;
use App\Util\XMLParser;
use App\Util\Errors\InternalError;
use App\Models\PeopleModel;
use App\Models\ShipOrderModel;

class ConsumeFileTaskValidProcess extends \Exception 
{
    public function __construct($message, $code = 201, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}

class ConsumeFileTask extends Task
{
    const STATUS_PROCESSAMENTO_CONCLUIDO  = "PROCESSAMENTO_CONCLUIDO";

    public function MainAction()
    {
        $this->listenQueue();
    }

    public function listenQueue()
    {
        // Resgatando conex�o
        $this->channel  = $this->getDi()->getShared('rabbit')->channel();

        // definindo fila padrao
        $exchange = $this->config->rabbit->exchange;
        $queue    = $this->config->rabbit->queue;

        // definindo fila reprocessamento
        $exchangeRetry = $this->config->rabbit->exchange_retry;
        $retryQueue    = $this->config->rabbit->queue_retry;

        $this->channel->exchange_declare($exchange, 'direct', false, true);
        $this->channel->exchange_declare($exchangeRetry, 'direct', false, true);

        // Fila normal e a da reenvio        
        $this->channel->queue_declare($queue, false, true, false, false, false, new \PhpAmqpLib\Wire\AMQPTable([
            'x-dead-letter-exchange' => '',
            'x-dead-letter-routing-key' => $retryQueue
        ]));

        $this->channel->queue_bind($queue, $exchange);

        // A cada 30 segundos as mensagens da fila de reprocessamento ser�o lidas
        $ttlMessage = $this->config->rabbit->ttl_message;
        $this->channel->queue_declare($retryQueue, false, true, false, false, false, new \PhpAmqpLib\Wire\AMQPTable([
            'x-dead-letter-exchange' => '',
            'x-dead-letter-routing-key' => $queue,
            'x-message-ttl' => $ttlMessage
        ]));
        $this->channel->queue_bind($retryQueue, $exchangeRetry);

        $this->channel->basic_qos(null, 1, null);
        $this->channel->basic_consume($queue, '', false, false, 
        false, false, array($this,'receiverMessage'));

        try {
            echo '[*] Waiting for messages. To exit press CTRL+C', "\n";
        
            while (count($this->channel->callbacks)) {
                $this->channel->wait();
            }
        } catch(\PhpAmqpLib\Exception\AMQPBasicCancelException $e) {
            echo utf8_encode($e->getMessage());
        }
    }

    public function receiverMessage(\PhpAmqpLib\Message\AMQPMessage $message) 
    {
        try {

            echo "[*] Received/Read: {$message->body} on date " . date('c') .  PHP_EOL;
            
            if ($this->processFiles($message)) {
                throw new ConsumeFileTaskValidProcess ("[^_^] Processed " . PHP_EOL);
            }
           
            throw new InternalError ("[x] Failure " . PHP_EOL);

        } catch(InternalError $e) {
            echo $e->getMessage() . PHP_EOL;
            // A mensagem sempre permanece na fila, s� e retirada quando ocorre alguma excess�o
            $message->nack();
        } catch(ConsumeFileTaskValidProcess $e) {
            echo $e->getMessage() . PHP_EOL;
            // Processa a mensagem e retira da fila e avisa o usuario do motivo
            $message->ack();
        } catch(\PhpAmqpLib\Exception\AMQPIOException $e) {
            echo $e->getMessage() . PHP_EOL;
            // A mensagem sempre permanece na fila, s� e retirada quando ocorre alguma excess�o
            $message->nack();
        }
    }

    public function processFiles(\PhpAmqpLib\Message\AMQPMessage $message)
    {
        // Extraindo os arquivos
        $dirUploads = $this->config->application->uploadDir;
        $pathFile   = $dirUploads . '/'. $message->body;
        $files      = ['people.xml','shiporders.xml'];
        $folderCreate = (new FileExtract())->extractTo($pathFile,$dirUploads,$files);

        if (empty($folderCreate)) {
            throw new InternalError ('Os arquivos n�o foram extraidos');
        }

        // Converte para Array
        $peoples = ((new XMLParser())->toArray($folderCreate . "/people.xml"));
        $shipOrders = ((new XMLParser())->toArray($folderCreate . "/shiporders.xml"));

        if (empty($peoples) || empty($shipOrders)) {
            throw new InternalError ('Os arquivos n�o foram parseados');
        }

        // Insere os dados de people
        $result = (new PeopleModel)->insert($peoples,$message->body);
        if ($result === false) {
            throw new InternalError ('Falha ao inserir os dados de people');
        }

        // Insere os dados de shipoder
        $result = (new ShipOrderModel)->insert($shipOrders,$message->body);
        if ($result === false) {
            throw new InternalError ('Falha ao inserir os dados de shiporder');
        }

        // Remove do redis a key
        $objRedis = $this->getDi()->getShared('redis');
        if ($objRedis->set($message->body, self::STATUS_PROCESSAMENTO_CONCLUIDO) === false) {
            throw new InternalError ('Falha ao remover chave do redis');
        }

        return true;
    }
}