$(document).on("submit", "form", function(event)
{
    event.preventDefault();

    // token da api
    const accessToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7Im5hbWUiOiJDYXJsb3MgQXVndXN0byIsImFkbWluIjp0cnVlfSwiaXNzIjoiaHR0cDpcL1wvbG9jYWxob3N0OjgwMjAiLCJzdWIiOiIxNDI1MzYifQ.XJRbGBUqVbOhv9X_aNgUaN-g9hJXlmqmWYD3GwNTdjY';

    $.ajax({
        url: $(this).attr("action"),
        type: $(this).attr("method"),
        headers: {"access-token": accessToken},
        dataType: "JSON",
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function (data, status){
            const stringified = JSON.stringify(data);
            const parsedObj   = JSON.parse(stringified);
            console.log(parsedObj);
            if (parsedObj.statusCode === 202 || parsedObj.statusCode === 201) {
                disponibilizaLink(parsedObj.data.filename);
                // limpa o formulario
                $("form").get(0).reset();
            } else {
                alert('Falha ao enviar o arquivo para o servidor');
            }
        },
        error: function (xhr, desc, err) {
            const parsedObj   = JSON.parse(xhr.responseText);
            console.log(parsedObj.message);
        }
    });
});

function disponibilizaLink(fileName)
{
    $('.mensagem-de-sucesso').show();
    $('.endereco-consulta-andamento').html("https://localhost:8020/upload/" + fileName);
}