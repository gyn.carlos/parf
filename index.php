<?php

ini_set('display_errors',-1);
ini_set('display_errors_startup',-1);
error_reporting(E_ALL);

require_once __DIR__ . "/vendor/autoload.php";

use Phalcon\Mvc\Micro;
use Phalcon\Http\Response;
use Phalcon\Di\FactoryDefault;

use Phalcon\Logger;
use Phalcon\Logger\Adapter\Stream;

use Phalcon\Config;

$di = new FactoryDefault();

// config
$config = require_once __DIR__ . '/app.config.php';
$di->set('config', $config);

// logger
$di->set('logger', function () use ($config) {
    $adapter = new Stream($config->application->logsDir. date('Y-m-d') .".txt");
    return new Logger('messages',['main' => $adapter]);
});

// mysql
$di->setShared('mysql', function() use ($config) {
    return new \Phalcon\Db\Adapter\Pdo\Mysql([
        "host"     => $config->mysql->host,
        "username" => $config->mysql->username,
        "password" => $config->mysql->password,
        "dbname"   => $config->mysql->dbname,
        "port"     => $config->mysql->port
    ]);
});

// rabbitmq
$di->setShared('rabbit', function() use ($config) {
    return new \PhpAmqpLib\Connection\AMQPConnection(
        $config->rabbit->host, 
        $config->rabbit->port, 
        $config->rabbit->username,
        $config->rabbit->password
    );
});

// redis
$di->setShared('redis', function() use ($config) {
    $connection = new \Redis;
    $connection->connect($config->redis->host, $config->redis->port);
    $connection->auth($config->redis->password);
    return $connection;
});

// load config
$loader = (new \Phalcon\Loader())->registerDirs([
    'App\Controllers' => $config->application->controllersDir,
    'App\Models' => $config->application->modelsDir,
    'App\Util' => $config->application->utilDir,
    'App\Tasks' => $config->application->tasksDir,
    'App\Middlewares' => $config->application->middlewaresDir,
])->register();

$di->get('dispatcher')->setDefaultNamespace('App\Tasks');

// init app
$app = new Micro($di);

$app->get("/",function () use ($app) {
        $app->logger->info('App run');
        $app->response->setContent("Ol�, seja bem vindo para projeto backend feito para Invillia.com");
        $app->response->send();
    }
);

$app->post("/upload", [new App\Controllers\UploadFileController, "upload"]);
$app->get ("/upload/{file_name_upload}"    , [new App\Controllers\UploadFileController , "status"]);

$app->get ("/people/{file_name_upload}"    , [new App\Controllers\PeopleController     , "showRows"]);
$app->get ("/shiporder/{file_name_upload}" , [new App\Controllers\ShipOrderController  , "showRows"]);


$app->error(
    function ($exception) {
        echo json_encode(
            [
                'code'    => $exception->getCode(),
                'status'  => 'error',
                'message' => $exception->getMessage(),
            ]
        );
    }
);

$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    $app->response->setContent(json_encode([
        'code' => '400',
        'status'  => 'error',
        'message' => 'Not found'
    ]));
    $app->response->send();
});

$app->handle($_SERVER["REQUEST_URI"]);