#!/bin/bash

# Adiciona a pasta upload ao grupo do apache
chgrp www-data uploads
chmod 775 -R uploads

# Adiciona a pasta logs ao grupo do apache
chgrp www-data logs
chmod 775 -R logs
