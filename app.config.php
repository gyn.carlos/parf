<?php
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__)));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/src');

return new \Phalcon\Config([
    'application' => [
        'appDir'         => APP_PATH . '/',
        'controllersDir' => APP_PATH . '/Controllers/',
        'tasksDir'       => APP_PATH . '/Tasks/',
        'modelsDir'      => APP_PATH . '/Models/',
        'utilDir'        => APP_PATH . '/Util/',
        'middlewareDir'  => APP_PATH . '/Middlewares/',        
        'migrationsDir'  => APP_PATH . '/Migrations/',
        'libraryDir'     => APP_PATH . '/Library/',
        'uploadDir'      => BASE_PATH . '/uploads/',
        'logsDir'        => BASE_PATH . '/logs/',
        'baseUri'        => '/src/',
    ],
    'mysql' => [
        'adapter'     => 'Mysql',
        'host'        => '172.30.0.2',
        'username'    => 'root',
        'password'    => 'mysql3241',
        'dbname'      => 'bulk',
        'charset'     => 'utf8mb4',
    ],
    'rabbit' => [
        'host' => '172.30.0.3',
        'port' => '5672',
        'username' => 'rabbit',
        'password' => 'rabbit3241',
        'exchange'  => 'invilla.processfiles.default',
        'queue' => 'invilla.processfiles.default',
        'exchange_retry'  => 'invilla.processfiles.retry',        
        'queue_retry' => 'invilla.processfiles.retry',
        'ttl_message' => 60000
    ],
    'redis' => [
        'host' => '172.30.0.4',
        'port' => '6379',
        'password' => 'redis3241'
    ]
]);